package com.assignment.presentation.helpers;

public interface IInternetStatus {
    boolean isConnected();
}
