package com.assignment.common;

public class Constants {

    public static class Extras {
        public static final String PHOTOS = "photos";
        public static final String SELECTED_POSITION = "selected_position";
    }

    public static class AppConstants {
        public static final String FOLDER_CACHE_APP_CAPTURED = ".assignment";
        public static final String EXTENSION = "jpg";
    }
}
